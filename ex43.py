from sys import exit
from random import randint

class Scene(object):

	def enter(self):
		print("This scene is not yet configured. Subclass it and implement enter().")
		
class Engine(object):

	def __init__(self, scene_map):
		self.scene_map = scene_map
	
	def play(self):
		current_scene = self.scene_map.opening_scene()
		
		while True:
			print "\n-----------"
			next_scene_name = current_scene.enter()
			current_scene = self.scene_map.next_scene(next_scene_name)
	
class Death(Scene):
	
	quips = [
		"You died. You kinda suck at this.",
		"Your mom would be proud.. if she were smarter.",
		"Such a loser.",
		"I have a small puppy that's better than this."]
		
	def enter(self):
		print Death.quips[randint(0, len(self.quips - 1))]
		exit(1)
		
class CentralCorridor(Scene):
	
	def enter(self):
		print "The Gothons of Planet Percal #25 have invaded your ship and destroyed"
		print "your entire crew. You are the last surviving member and your last"
		print "mission is to get the neutron destruct bomb from the Weapons Armory,"
		print "put it on the bridge, and blow the ship up after getting into an"
		print "escape pod."
		print "\n"
		print "You're running down the central corridor to the Weapons Armory then"
		print "a Gothon jumps out, red scaly skin, dark grimy teeth, and evil clown costume"
		print "flowing around his hate filled body. He's blocking the door to the"
		print "Armory and about to pull a weapon to blast you."
		
		action = raw_input("> ")
		
		if action == "shoot!":
			print "He blasts you"
			return 'death'
		
		elif action == "dodge!":
			print "Your foot slips and he blasts you"
			return 'death'
		
		elif action == "tell a joke":
			print "He laughs and you jump through the Weapon Armory door."
			return 'laser_weapon_armory'
		
		else:
			print "DOES NOT COMPUTE!"
			return 'central_corridor'
		
class LaserWeaponArmory(Scene):
	
	def enter(self):
		print "You've entered this room."
		print "Neuron Bomb is in its container and there's a keypad lock on the box."
		print "You need the code to get it out, if you've got the code wrong 10 times,"
		print "the box will be locked forever."
		print "the code is 3 digits"
		code = "%d%d%d" % (randint(1,9), randint(1,9), randint(1,9))
		guess = raw_input("[keypad]> ")
		guesses = 0
		
		while guess != code and guesses < 10:
			print "BZZEEEDD"
			guesses += 1
			guess = raw_input("[keypad]> ")
		
		if guess == code:
			print "you've got a neuron bomb"
			return 'the_bridge'
		
		else:
			print "the locked buzzes one last time and gothons come to blast you"
			return 'death'
		
	
class TheBridge(Scene):
	
	def enter(self):
		print "You're on the bridge"
		print "The group of gothans are coming to you"
		
		action = raw_input("> ")
		
		if action == "throw the bomb":
			print "In a panic you throw it to the group of gothans and they blast you"
			return 'death'
		
		elif action == "slowly place the bomb":
			print "you've got to the pod"
			return 'escape_pod'
		
		else:
			print "DOES NOT COMPUTE"
			return 'the_bridge'
	
class EscapePod(Scene):
	
	def enter(self):
		print "You rush to the pod. But there are 5 pods that you have to choose one of them"
		good_pod = randint(1,5)
		guess = raw_input("> ")
		
		if int(guess) != good_pod:
			print "you jump into %s pod and you're going to the void of space" % guess
			return 'death'
		
		else:
			print "you jump into %s pod and you're going to the safe planet" % guess
			print "You won the game!"
			return 'finished'
	
class Map(object):
	
	scenes = {
		'central_corridor' : CentralCorridor(),
		'laser_weapon_armory' : LaserWeaponArmory(),
		'the_bridge' : TheBridge(),
		'escape_pod' : EscapePod(),
		'death' : Death()}
	
	def __init__(self, start_scene):
		self.start_scene = start_scene
	
	def next_scene(self, scene_name):
		return Map.scenes.get(scene_name)
	
	def opening_scene(self):
		return self.next_scene(self.start_scene)
	
a_map = Map('central_corridor')
a_game = Engine(a_map)
a_game.play()