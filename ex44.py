#imply inheritance

class Parent(object):
	def implicit(self):
		print "Parent Implicit()"
		
class Child(Parent):
	pass

dad = Parent()
son = Child()

dad.implicit()
son.implicit()

#override inheritance

class Besar(object):
	def override(self):
		print "Besar dicoba"
	
class Kecil(Besar):
	def override(self):
		print "Kecil dicoba"
		
bs = Besar()
kc = Kecil()

bs.override()
kc.override()

#alter inheritance

class Panjang(object):
	def altered(self):
		print "Panjang nih"
	
class Pendek(Panjang):
	def altered(self):
		print "Pendek Sebelum Panjang"
		super(Pendek, self).altered()
		print "Pendek Setelah Panjang"
	
pj = Panjang()
pd = Pendek()

pj.altered()
pd.altered()

#composition

class Other(object):
	def implicit(self):
		print "Other implicit"
	
	def altered(self):
		print "Other altered"
	
	def override(self):
		print "Other override"
	
class Anak(Other):
	def __init__(self):
		self.other = Other()
	
	def implicit(self):
		self.other.implicit()
	
	def altered(self):
		print "Anak before Other altered"
		self.other.altered()
		print "Anak after Other altered"
	
	def override(self):
		print "Anak override"
		
sn = Anak()

sn.implicit()
sn.altered()
sn.override()
